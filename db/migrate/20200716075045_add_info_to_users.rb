class AddInfoToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :gender, :integer
    add_column :users, :age, :integer
    add_column :users, :have_house_or_not, :integer
    add_column :users, :sensitiveness, :integer
    add_column :users, :payable_amount, :integer
    add_column :users, :comment, :text
  end
end
