User.create!(name: "Example User",
              gender: 1,
              age: 25,
              payable_amount: 3,
              have_house_or_not: 1,
              prefecture_id: 10,
              sensitiveness: 3,
              comment: "よろしくお願いします。",
              email: "example@railstutorial.org",
              password:              "foobar",
              password_confirmation: "foobar",
              admin:     true,
              activated: true,
              activated_at: Time.zone.now)

99.times do |n|
name  = Faker::Name.name
gender = rand(1..3)
age = rand(101)
payable_amount = [3, 4, 5, 6, 7, 8, 9, 12, 15, 16].sample
have_house_or_not = rand(1..2)
prefecture_id = rand(47)
sensitiveness = rand(1..5)
comment = Faker::Lorem.sentence
email = "example-#{n+1}@railstutorial.org"
password = "password"
User.create!(name:  name,
            gender: gender,
            age: age,
            payable_amount: payable_amount,
            have_house_or_not: have_house_or_not,
            prefecture_id: prefecture_id,
            sensitiveness: sensitiveness,
            comment: comment,
            email: email,
            password:              password,
            password_confirmation: password,
            activated: true,
            activated_at: Time.zone.now)
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
