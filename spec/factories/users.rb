FactoryBot.define do
  factory :user do
    name { "Example" }
    gender { :男性 }
    age { 25 }
    payable_amount { :"3万円以下" }
    have_house_or_not { :決まっている }
    prefecture_id { 0 }
    sensitiveness { :キレイ好き }
    comment { "よろしくお願いします。" }
    sequence(:email) { |n| "tester#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
    activated { true }
  end
end
