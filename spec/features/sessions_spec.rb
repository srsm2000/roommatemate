require 'rails_helper'

RSpec.describe 'Sessions', type: :feature do
  before do
    visit login_path
  end
  #無効な値を入力する
  describe 'enter an invalid values' do
    before do
      fill_in "メールアドレス", with: ''
      fill_in 'パスワード', with: ''
      click_button 'ログイン'
    end
    subject { page }
    #フラッシュメッセージが出る
    it 'gets an flash messages' do
      is_expected.to have_selector('.alert-danger', text: 'メールアドレスとパスワードが一致しません')
      is_expected.to have_current_path login_path
    end
    #違うページにアクセスしたとき
    context 'access to other page' do
      before { visit root_path }
      #フラッシュメッセージが消える
      it 'is flash disappear' do
        is_expected.to_not have_selector('.alert-danger', text: 'メールアドレスとパスワードが一致しません')
      end
    end
  end
end