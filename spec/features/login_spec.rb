require 'rails_helper'

RSpec.feature "Login", type: :feature do
  let!(:user) { FactoryBot.create(:user) }
  let!(:prefecture) { FactoryBot.create(:prefecture) }
  # ログインに成功すること
  scenario "user successfully login and logout" do
    valid_login(user)

    expect(current_path).to eq user_path(user)
    expect(page).to_not have_content "ログイン"

    click_link "ログアウト"
    expect(current_path).to eq root_path
    expect(page).to_not have_content "ログアウト"
  end

  # 無効な情報ではログインに失敗すること
  scenario "user doesn't login with invalid information" do
    visit login_path
    fill_in "メールアドレス", with: ""
    fill_in "パスワード", with: ""
    click_button "ログイン"

    expect(current_path).to eq login_path
    expect(page).to have_content "ログイン"
    expect(page).to have_content "メールアドレスとパスワードが一致しません"
  end
end