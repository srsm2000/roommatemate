require 'rails_helper'

RSpec.feature "StaticPages", type: :feature do
  feature "Home page" do
    before do
      visit root_path
    end

    feature "タイトル"
    include ApplicationHelper
    scenario "should have the right title" do
      expect(page).to have_title full_title('')
    end
  end

  feature "Help page" do
    before do
      visit help_path
    end

    feature "タイトル"
    include ApplicationHelper
    scenario "should have the right title" do
      expect(page).to have_title full_title('Help')
    end
  end
end