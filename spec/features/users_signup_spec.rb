require 'rails_helper'

RSpec.feature "UsersSignup", type: :feature do

  scenario "invalid signup information" do
    visit signup_path
    expect {
      fill_in 'ユーザーネーム', with: ''
      fill_in 'メールアドレス', with: ''
      fill_in 'パスワード', with: ''
      fill_in 'パスワード確認', with: ''
      click_button 'アカウントを作成する'
    }.not_to change(User, :count)
    expect(page).to have_content "エラーがあります。"
    expect(current_path).to eq signup_path
  end

  scenario "valid signup information" do
    visit signup_path

    expect {
      fill_in 'ユーザーネーム', with: "Example User"
      select 1, from: "年齢"
      choose '男性'
      choose '決まっている'
      fill_in 'メールアドレス', with: "user@example.com"
      fill_in 'パスワード', with: "password"
      fill_in 'パスワード確認', with: "password"
      click_button 'アカウントを作成する'
    }.to change(User, :count).by(1)
    expect(page).to have_content "メールをご確認下さい。"
    expect(current_path).to eq root_path
    # expect(page).to have_content "登録が完了しました！"
    # expect(current_path).to eq "/users/1"
    # expect(page).to have_content "ログアウト"
  end
end