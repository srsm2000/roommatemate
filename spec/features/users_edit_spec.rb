require 'rails_helper'

RSpec.feature "UsersEdit", type: :feature do
  let!(:user) { FactoryBot.create(:user) }
  let!(:prefecture) { FactoryBot.create(:prefecture) }

  scenario "invalid update information" do
    valid_login(user)
    visit edit_user_path(user)
    fill_in 'ユーザーネーム', with: ''
    fill_in 'メールアドレス', with: ''
    fill_in 'パスワード', with: ''
    fill_in 'パスワード確認', with: ''
    click_button 'プロフィールを変更する'

    expect(page).to have_content "エラーがあります。"
    expect(current_path).to eq user_path(user)
  end

  # ユーザーは編集に成功する
  scenario "successful edit" do
    visit edit_user_path(user)
    valid_login(user)
    expect(current_path).to eq edit_user_path(user)
    # click_link "プロフィールを変更する"

    fill_in "メールアドレス", with: "edit@example.com"
    fill_in "パスワード", with: "test123", match: :first
    fill_in "パスワード確認", with: "test123"
    click_button "プロフィールを変更する"

    expect(current_path).to eq user_path(user)
    expect(user.reload.email).to eq "edit@example.com"
    # expect(user.reload.password).to eq "test123"
  end
end
