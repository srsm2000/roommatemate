require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  it "response signup page" do
    visit signup_path
    expect(response).to have_http_status(:ok)
  end
end
