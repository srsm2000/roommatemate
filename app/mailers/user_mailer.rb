class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Roommate Mate 仮登録完了のお知らせ"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Roommate Mate パスワード再設定のリンクをお送りします"
  end
end
