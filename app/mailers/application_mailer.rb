class ApplicationMailer < ActionMailer::Base
  default from: "noreply@roommatemate.com", charset: 'ISO-2022-JP'
  layout 'mailer'
end
