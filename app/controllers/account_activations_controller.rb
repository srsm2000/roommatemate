class AccountActivationsController < ApplicationController

  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "アカウントが作成されました。まずはプロフィールを完成させましょう！"
      redirect_to user
    else
      flash[:danger] = "無効なリンクです。お手数ですがリンクをご確認ください。"
      redirect_to root_url
    end
  end
end
